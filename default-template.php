<!DOCTYPE html>
<html lang="de">
  <head>
    <?php include_once ($serverRoot.'/partials/head.php'); ?>
  </head>
  <body>
      <div id="fullpage" class="site-wrapper">
        <div class="section" class="content-wrapper">
            <?php include_once ($serverRoot.'/controller/ajax-load-content.php'); ?>
        </div>
      </div>
			<?php include_once  ($serverRoot.'/partials/nav.html'); ?>
  </body>
</html>
