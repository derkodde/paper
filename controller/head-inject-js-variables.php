<?php

/**
 * @param $jsVars
 * @return false|string
 */
function headInjectJsVariables($jsVars)
{
    ob_start();
    echo '<script>';
    echo 'const PAPER = {};';

    foreach ($jsVars as $key => $value) {
        echo "PAPER.{$key} = '{$value}';";
    }

    echo '</script>';
    return ob_get_clean();
}
