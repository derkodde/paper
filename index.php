<?php 
    include_once 'config.php';
    $page = 'home';	
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <?php include_once ('partials/head.php'); ?>
  </head>
  <body>
      <div id="fullpage" class="site-wrapper">
        <div class="section" class="content-wrapper">
            <?php include_once ($path.'/controller/ajax-load-content.php'); ?>
        </div>
      </div>
			<?php include_once '/partials/nav.html'; ?>
  </body>
</html>
