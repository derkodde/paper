<?php 
$root = '/';
$serverRoot = $_SERVER['DOCUMENT_ROOT'].$root;
$domain = 'https://paper.ddev.site';
$webRoot = $domain.$root;

$jsVars = [
    'webRoot'=> $webRoot
];

//load classes
$classes = [
    'controller/head-inject-js-variables.php'
];
foreach ($classes as $classPath) {
    include_once ($serverRoot.$classPath);
}