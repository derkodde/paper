<title>paper - <?php echo $page; ?> </title>
<base href="<?php echo $webRoot; ?>">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<!--320-->

<!--css-->
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.fullpage.css">
<link rel="stylesheet" href="css/main.css">

<!--inject js Variables-->
<?php echo headInjectJsVariables($jsVars); ?>

<!--js-->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery.fullpage.js"></script>
<script src="js/scrolloverflow.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src='tinymce/tinymce.min.js'></script>
<script src="js/tinymce-conf.js"></script>
<script src="js/attrchange.js"></script>
<script src="js/functions.js"></script>
<script src="js/paper.js"></script>

